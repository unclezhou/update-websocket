package com.websocket.websocket.redis.action;


import com.alibaba.fastjson.JSONObject;
import com.websocket.websocket.WebSocketManager;


public class RemoveAction implements Action{
    @Override
    public void doMessage(WebSocketManager manager, JSONObject object) {
        if(!object.containsKey(IDENTIFIER)){
            return;
        }

        String identifier = object.getString(IDENTIFIER);
        manager.remove(identifier);
    }
}
