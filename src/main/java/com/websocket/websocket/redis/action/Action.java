package com.websocket.websocket.redis.action;


import com.alibaba.fastjson.JSONObject;
import com.websocket.websocket.WebSocketManager;


public interface Action {
    String IDENTIFIER = "identifier";
    String USER_ACCOUNT = "userAccount";
    String MESSAGE    = "message";
    String ACTION     = "action";

    void doMessage(WebSocketManager manager, JSONObject object);
}
