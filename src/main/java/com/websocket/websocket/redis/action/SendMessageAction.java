package com.websocket.websocket.redis.action;


import com.alibaba.fastjson.JSONObject;
import com.websocket.websocket.WebSocket;
import com.websocket.websocket.WebSocketManager;
import com.websocket.websocket.utils.WebSocketUtil;


import java.util.List;


public class SendMessageAction implements Action{
    @Override
    public void doMessage(WebSocketManager manager, JSONObject object) {
        if(!object.containsKey(USER_ACCOUNT)){
            return;
        }
        if(!object.containsKey(MESSAGE)){
            return;
        }

        String userAccount = object.getString(USER_ACCOUNT);

        List<WebSocket> list = manager.getList(userAccount);
        if(list==null||list.size()==0){
            return;
        }
        for (WebSocket webSocket:list) {
            WebSocketUtil.sendMessageAsync(webSocket.getSession() , object.getString(MESSAGE));
        }
    }
}
