package  com.websocket.websocket.redis;


import com.websocket.websocket.WebSocket;
import com.websocket.websocket.memory.MemWebSocketManager;
import com.websocket.websocket.redis.action.Action;
import com.websocket.websocket.redis.action.SendMessageAction;
import com.websocket.websocket.utils.JsonUtil;
import com.websocket.websocket.utils.ResponseData;
import org.springframework.data.redis.core.RedisTemplate;

import java.io.Serializable;
import java.util.HashMap;
import java.util.Map;


public class RedisWebSocketManager extends MemWebSocketManager {
    public static final String CHANNEL = "websocket";
    protected RedisTemplate<Serializable, Serializable> redisTemplate;

    public RedisWebSocketManager(RedisTemplate<Serializable, Serializable> redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    @Override
    public void put(String identifier, WebSocket webSocket) {
        super.put(identifier, webSocket);
    }

    @Override
    public void remove(String identifier) {
        super.remove(identifier);
    }

    @Override
    public void sendMessage(String userAccount, String message) {
        Map<String, Object> map = new HashMap<>(3);
        map.put(Action.ACTION, SendMessageAction.class.getName());
        map.put(Action.USER_ACCOUNT, userAccount);
        ResponseData responseData = new ResponseData("message", message);
        map.put(Action.MESSAGE, responseData);
        // 发布消息到redis频道上 redis转发到订阅的各个socket实例上 收到信息 根据标识 获取到session 发给自己对应的客户端
        redisTemplate.convertAndSend(getChannel(), JsonUtil.serializeMap(map));
    }


    protected String getChannel() {
        return CHANNEL;
    }
}
