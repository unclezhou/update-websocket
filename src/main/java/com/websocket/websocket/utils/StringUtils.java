package com.websocket.websocket.utils;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.UUID;

public class StringUtils {
    public static String uuid() {
        return UUID.randomUUID().toString().replaceAll("-", "").toUpperCase();
    }

    public static boolean isEmpty(String str) {
        return str == null || str.isEmpty();
    }

    public static boolean isNotEmpty(String cs) {
        return cs != null && !cs.isEmpty();
    }

    /**
     * * 判断一个对象是否为空
     *
     * @param object Object
     * @return true：为空 false：非空
     */
    public static boolean isNull(Object object)
    {
        return object == null;
    }

    /**
     * 是否包含字符串
     *
     * @param str 验证字符串
     * @param strs 字符串组
     * @return 包含返回true
     */
    public static boolean inStringIgnoreCase(String str, String... strs)
    {
        if (str != null && strs != null)
        {
            for (String s : strs)
            {
                if (str.equalsIgnoreCase(trim(s)))
                {
                    return true;
                }
            }
        }
        return false;
    }

    /**
     * 去空格
     */
    public static String trim(String str)
    {
        return (str == null ? "" : str.trim());
    }

    /**
     * * 判断一个对象数组是否非空
     *
     * @param objects 要判断的对象数组
     * @return true：非空 false：空
     */
    public static boolean isNotEmpty(Object[] objects)
    {
        return !isEmpty(objects);
    }

    /**
     * * 判断一个对象数组是否为空
     *
     * @param objects 要判断的对象数组
     ** @return true：为空 false：非空
     */
    public static boolean isEmpty(Object[] objects)
    {
        return isNull(objects) || (objects.length == 0);
    }


    /**
     * 字母加数字转字母
     */
    public static String char2String(int shuzhi){
        String key = null;
        if(shuzhi<26){
            key = Character.toString((char) ('A' + shuzhi));
        }else{
            // 计算次数
            int times=shuzhi/26-1;
            key = Character.toString((char) ('A' + times)) + (char) ('A' + shuzhi-26*(times+1));
        }
      return  key;
    }


    /**
     * 根据map的value获取map的key
     * 注意：value相同的值有很多个，都会对应到第一个找到的key上，因此要把找到的key标记，下次不再用
     */
    public static String getKey(Map<String,Integer> map, Integer value){
        String key="";
        List<String> containedKey=new ArrayList<>();
        //遍历map
        for (Map.Entry<String, Integer> entry : map.entrySet()) {
            //如果value和key对应的value相同 并且 key不在list中
            if(value==entry.getValue() && (!containedKey.contains(entry.getKey()))){
                key=entry.getKey();
                containedKey.add(entry.getKey());
                break;
            }
        }
        return key;
    }


    public static void main(String[] args) {
        System.out.println(char2String(52));
    }

}
