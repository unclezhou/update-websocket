package com.websocket.websocket.utils;

import java.util.Collection;

public class CollectionUtils {
    public static boolean isEmpty(Collection collection) {
        return collection == null || collection.isEmpty();
    }

    public static boolean isNotEmpty(Collection collection) {
        return collection != null && !collection.isEmpty();
    }

    public static void addStringElement(Collection<String> list, String str) {
        if (StringUtils.isNotEmpty(str)) {
            list.add(str);
        }
    }

    public static void addObjectElement(Collection<Object> list, Object obj) {
        if (obj != null) {
            list.add(obj);
        }
    }

}
